﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmMan_Controllers
{
    public class CowInfoManager
    {
        private List<int> indexes = new List<int>();

        public string Covert_Raw_ZbID(string RawData)
        {
            AllIndexesOf(RawData, '*');
            string ZbID = "";
            ZbID = RawData.Substring(indexes[0] + 1, indexes[1] - indexes[0] - 1);
            return ZbID;
        }

        public double Covert_Raw_Lat(string RawData)
        {
            AllIndexesOf(RawData, '*');
            double _latValue;
            _latValue = Double.Parse(RawData.Substring(indexes[1] + 1, indexes[2] - indexes[1] - 1));
            int lattemp1 = Convert.ToInt16(_latValue / 100);
            double lattemp2 = _latValue - (lattemp1 * 100);
            lattemp2 = lattemp2 / 60;
            return lattemp1 + lattemp2;
        }

        public double Covert_Raw_Lon(string RawData)
        {
            double _lngValue;
            _lngValue = Double.Parse(RawData.Substring(indexes[2] + 1, RawData.Length - indexes[2] - 2));
            int lontemp1 = Convert.ToInt16(_lngValue / 100);
            double lontemp2 = _lngValue - (lontemp1 * 100);
            lontemp2 = lontemp2 / 60;
            return lontemp1 + lontemp2;
        }

        public void AllIndexesOf(string str, char value)
        {
            indexes = new List<int>();
            for (int index = 0; index < str.Length; index++)
            {
                index = str.IndexOf(value, index);
                if (index != -1)
                    indexes.Add(index);
                else
                    break;
            }
        }
    }
}
