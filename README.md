#*** Farm Man Software ***#


##1. Purpose: ##
   - Manage cows (up to 1000)

   - Show each cow a marker on Bing Map, as well as his previous position (maybe as vector)

   - Can receive Android message "Cow ID", and make that cow's led blinking 

##2. Structure: ##

   - Database: Basic Access database for the ease of usage

   - Model: Gonna have the same members as the database. We will store temporary each cow's info into a Model-Type-Class and push this data on to database if all required info is received.

   - Controller: provide processes such as 

       + convert raw data --> usable information

       + method of updating and accessing database

   - View: View Cows positions as marker on the map. Configure UART connection. Send data request, receive data from FarmMan's Coordinator.


##3. Members' remark: ##

   - Firmware developer: should support the communication between Coordinator and Software.

   - Software developer: build a well-constructed, easy-to-understand, reliable Software.


##4. Good luck, have fun :)##