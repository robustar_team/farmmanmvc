﻿namespace FarmManMVC
{
    partial class FarmManUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FarmMap = new GMap.NET.WindowsForms.GMapControl();
            this.Menu_FarmMan = new System.Windows.Forms.MenuStrip();
            this.connectDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Timer_ReadData = new System.Windows.Forms.Timer(this.components);
            this.Timer_DataRequest = new System.Windows.Forms.Timer(this.components);
            this.Menu_FarmMan.SuspendLayout();
            this.SuspendLayout();
            // 
            // FarmMap
            // 
            this.FarmMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FarmMap.Bearing = 0F;
            this.FarmMap.CanDragMap = true;
            this.FarmMap.EmptyTileColor = System.Drawing.Color.Navy;
            this.FarmMap.GrayScaleMode = false;
            this.FarmMap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.FarmMap.LevelsKeepInMemmory = 5;
            this.FarmMap.Location = new System.Drawing.Point(12, 43);
            this.FarmMap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FarmMap.MarkersEnabled = true;
            this.FarmMap.MaxZoom = 2;
            this.FarmMap.MinZoom = 2;
            this.FarmMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.FarmMap.Name = "FarmMap";
            this.FarmMap.NegativeMode = false;
            this.FarmMap.PolygonsEnabled = true;
            this.FarmMap.RetryLoadTile = 0;
            this.FarmMap.RoutesEnabled = true;
            this.FarmMap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.FarmMap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.FarmMap.ShowTileGridLines = false;
            this.FarmMap.Size = new System.Drawing.Size(1303, 564);
            this.FarmMap.TabIndex = 0;
            this.FarmMap.Zoom = 0D;
            // 
            // Menu_FarmMan
            // 
            this.Menu_FarmMan.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.Menu_FarmMan.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectDeviceToolStripMenuItem,
            this.configDatabaseToolStripMenuItem});
            this.Menu_FarmMan.Location = new System.Drawing.Point(0, 0);
            this.Menu_FarmMan.Name = "Menu_FarmMan";
            this.Menu_FarmMan.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.Menu_FarmMan.Size = new System.Drawing.Size(1327, 28);
            this.Menu_FarmMan.TabIndex = 3;
            // 
            // connectDeviceToolStripMenuItem
            // 
            this.connectDeviceToolStripMenuItem.Name = "connectDeviceToolStripMenuItem";
            this.connectDeviceToolStripMenuItem.Size = new System.Drawing.Size(124, 24);
            this.connectDeviceToolStripMenuItem.Text = "Connect Device";
            this.connectDeviceToolStripMenuItem.Click += new System.EventHandler(this.connectDeviceToolStripMenuItem_Click);
            // 
            // configDatabaseToolStripMenuItem
            // 
            this.configDatabaseToolStripMenuItem.Name = "configDatabaseToolStripMenuItem";
            this.configDatabaseToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.configDatabaseToolStripMenuItem.Text = "Setting";
            // 
            // ReadTimer
            // 
            this.Timer_ReadData.Enabled = true;
            this.Timer_ReadData.Interval = 500;
            this.Timer_ReadData.Tick += new System.EventHandler(this.ReadTimer_Tick);
            // 
            // DataRequestTimer
            // 
            this.Timer_DataRequest.Interval = 500;
            this.Timer_DataRequest.Tick += new System.EventHandler(this.DataRequestTimer_Tick);
            // 
            // FarmManUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1327, 619);
            this.Controls.Add(this.Menu_FarmMan);
            this.Controls.Add(this.FarmMap);
            this.MainMenuStrip = this.Menu_FarmMan;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FarmManUI";
            this.Text = "Farm Man Control";
            this.Load += new System.EventHandler(this.FarmManUI_Load);
            this.Menu_FarmMan.ResumeLayout(false);
            this.Menu_FarmMan.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl FarmMap;
        private System.Windows.Forms.MenuStrip Menu_FarmMan;
        private System.Windows.Forms.ToolStripMenuItem connectDeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configDatabaseToolStripMenuItem;
        private System.Windows.Forms.Timer Timer_ReadData;
        private System.Windows.Forms.Timer Timer_DataRequest;
    }
}

