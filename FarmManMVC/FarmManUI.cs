﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FarmManMVC.Properties;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using FarmMan_Models;
using FarmMan_Controllers;

namespace FarmManMVC
{
    public partial class FarmManUI : Form
    {
        // The View must show the markers regarding to the cows location. 
        // It should also somehow show the previous location of the cow.

        // Data of our Cows stored with the formula defined in the Model

        // Create an overlay to draw markers on top
        GMapOverlay markersOverlay = new GMapOverlay();

        // Create an Object as Terminal Form
        frmTerminal Terminal = new frmTerminal();

        // Create FM_Controller to use methods in CowInfoManager
        CowInfoManager FM_Controller = new CowInfoManager();

        // access to setting
        private Settings setting = Settings.Default;

        public FarmManUI()
        {
            InitializeComponent();
            setting.Reload();

            // reset Timer if setting is changed
            if (Timer_DataRequest.Interval != setting.RequestInterval)
            {
                Timer_DataRequest.Interval = setting.RequestInterval;
                Timer_DataRequest.Stop();
                Timer_DataRequest.Start();
            }

            Terminal.FormClosing += new FormClosingEventHandler(Terminal_Closing);
        }

        // Load the map when start
        private void FarmManUI_Load(object sender, EventArgs e)
        {
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            //use google provider
            FarmMap.MapProvider = GMap.NET.MapProviders.BingMapProvider.Instance;
            //get tiles from server only
            FarmMap.Manager.Mode = AccessMode.ServerAndCache;
            //not use proxy
            GMapProvider.WebProxy = null;
            //center map on HCM
            FarmMap.Position = new PointLatLng(10.802519, 106.627125);

            //zoom min/max; default both = 2
            FarmMap.MinZoom = 10;
            FarmMap.MaxZoom = 20;
            //set zoom
            FarmMap.Zoom = 15;
            FarmMap.Update();

            //run timers
            Timer_DataRequest.Start();
            Timer_ReadData.Start();
        }

        // Switch to terminal UI
        private void connectDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Terminal.Show(this);
            connectDeviceToolStripMenuItem.Enabled = false;
        }

        // Timer write tick - request data
        private void DataRequestTimer_Tick(object sender, EventArgs e)
        {
            Terminal.SendAsInvoked();
        }
        // Timer read tick - read data
        private void ReadTimer_Tick(object sender, EventArgs e)
        {

        }

        #region Special EventHandling
        // Save the current state of Terminal and Hide it instead of Close
        void Terminal_Closing(object sender, FormClosingEventArgs e)
        {
            // Do something
            connectDeviceToolStripMenuItem.Enabled = true;

        }
        #endregion

        

        
    }
}
