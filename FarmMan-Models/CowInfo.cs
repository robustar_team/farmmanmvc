﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmMan_Models
{
    public class CowInfo
    {
        public string ZigBeeID { get; set; }
        public double Lat_Curr { get; set; }
        public double Lat_Prev { get; set; }
        public double Lon_Curr { get; set; }
        public double Lon_Prev { get; set; }
        public double MilkAmount { get; set; }  /* not yet implemented */
        public double EatTime { get; set; }     /* not yet implemented */
        public CowInfo(string ZbID, double Latitude, double Long
                        //, double milk, double eat            /* not yet implemented */
            )
        {
            ZigBeeID = ZbID;
            Lat_Curr = Latitude;
            Lon_Curr = Long;
        }
    }
}
